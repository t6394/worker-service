﻿using System;

namespace Worker_Service.Clients.MessageQueueClients
{
    public interface IMessageQueueClient
    {
        void Subscribe<T>(string queueName, Action<T> callback);
    }
}