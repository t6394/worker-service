﻿namespace Worker_Service.Models
{
    public class KafkaLogMessage
    {
        public string Path { get; set; }
        public string HttpMethod { get; set; }
        public int StatusCode { get; set; }
        public double ElapsedTime { get; set; }

    }
}