﻿using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Worker_Service.Clients.MessageQueueClients;
using Worker_Service.Models;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Worker_Service.Services
{
    public class WorkerService : BackgroundService
    {
        private readonly IMessageQueueClient _messageQueueClient;
        private readonly KafkaClient _kafkaClient;

        public WorkerService(IMessageQueueClient messageQueueClient, KafkaClient kafkaClient)
        {
            _messageQueueClient = messageQueueClient;
            _kafkaClient = kafkaClient;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _messageQueueClient.Subscribe(RabbitMQHelper.CreatedOrderQueue,new Action<Order>(o =>
                {
                    Console.WriteLine($"Created Order Queue:{o.Id} -> {o.Price} {o.Quantity}");
                }));
                
                _messageQueueClient.Subscribe(RabbitMQHelper.UpdatedOrderQueue,new Action<Order>(o =>
                {
                    Console.WriteLine($"Updated Order Queue:{o.Id} -> {o.Price} {o.Quantity}");
                }));
                
                _messageQueueClient.Subscribe(RabbitMQHelper.CreatedCustomerQueue,new Action<Customer>(c =>
                {
                    Console.WriteLine($"Created Customer Queue:  {c.Id} -> {c.Email}");
                }));
                
                _messageQueueClient.Subscribe(RabbitMQHelper.UpdatedCustomerQueue,new Action<Customer>(c =>
                {
                    Console.WriteLine($"Updated Customer Queue:  {c.Id} -> {c.Email}");
                }));

                var kafkaMessage = _kafkaClient.Subscribe(stoppingToken);
                var kafkaLogMessage = JsonConvert.DeserializeObject<KafkaLogMessage>(kafkaMessage.Message.Value);
                Console.WriteLine($"{JsonSerializer.Serialize(kafkaLogMessage)}");
                
                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}