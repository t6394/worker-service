﻿using System;

namespace Worker_Service.Models
{
    public class Customer
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Address Address { get; set; }
        public bool isDeleted { get; set; }
    }
}