﻿using System;

namespace Worker_Service.Models
{
    public class Product
    {
        public Guid Id { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }

    }
}