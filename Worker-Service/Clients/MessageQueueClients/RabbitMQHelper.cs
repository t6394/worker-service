﻿namespace Worker_Service.Clients.MessageQueueClients
{
    public class RabbitMQHelper
    {
        public static string CreatedCustomerQueue => "Created-Customer-Queue";
        public static string UpdatedCustomerQueue => "Updated-Customer-Queue";
        
        public static string CreatedOrderQueue => "Created-Order-Queue";
        public static string UpdatedOrderQueue => "Updated-Order-Queue";

    }
}