﻿using System.Threading;
using Confluent.Kafka;

namespace Worker_Service.Clients.MessageQueueClients
{
    public class KafkaClient
    {
        private readonly IConsumer<string, string> _consumer;

        public KafkaClient()
        {
            var config = new ConsumerConfig
            {
                BootstrapServers = "localhost:9092",
                GroupId = "Logs-group-0",
                AutoOffsetReset = AutoOffsetReset.Earliest
            };
            _consumer = new ConsumerBuilder<string, string>(config).Build();
            _consumer.Subscribe("Logs");
            _consumer.Assign(new TopicPartitionOffset("Logs",new Partition(0),Offset.Beginning));
        }
        
        public ConsumeResult<string,string> Subscribe(CancellationToken stoppingToken )
        {
            var cr = _consumer.Consume(stoppingToken);
            return cr;
        }
    }
}